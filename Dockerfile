ARG VERSION=5.7

FROM openjdk:18-slim-buster AS builder

RUN apt-get update && \
    apt-get install -y wget unzip git build-essential && \
    rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/facebookresearch/fastText.git && \
    cd fastText && \
    make
ARG VERSION
RUN wget https://www.languagetool.org/download/LanguageTool-$VERSION.zip && \
    unzip LanguageTool-$VERSION.zip && \
    rm LanguageTool-$VERSION.zip

FROM openjdk:16-slim-buster
ARG VERSION

RUN addgroup --system --gid 1000 languagetool && \
    adduser --system --ingroup languagetool --uid 1000 --shell /bin/sh --home /languagetool --no-create-home languagetool

USER languagetool

COPY --chown=languagetool:languagetool --from=builder /LanguageTool-$VERSION /languagetool
COPY --chown=languagetool:languagetool --from=builder /fastText/fasttext /languagetool/fasttext

WORKDIR /languagetool


ENTRYPOINT [ "java", "-cp", "languagetool-server.jar", "org.languagetool.server.HTTPServer" ]
